<?php

namespace Lkt\InstancePatterns;

/**
 * Trait Instantiable
 * @package Lkt\InstancePatterns
 * @deprecated
 */
trait Instantiable
{
    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        $r = new static(...$args);

        if (\method_exists($r, 'PostAwake')){
            $r->PostAwake();
        }

        return $r;
    }
}