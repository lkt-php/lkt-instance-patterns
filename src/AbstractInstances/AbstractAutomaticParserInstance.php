<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\AutomaticInstanceInterface;
use Lkt\InstancePatterns\Interfaces\ParserInterface;
use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;

/**
 * Class AbstractAutomaticHandlerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractAutomaticParserInstance implements ParserInterface, AutomaticInstanceInterface
{
    use AutomaticInstanceTrait;
}