<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractCreateCrud
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractCreateCrud extends AbstractAutomaticHandlerInstance
{
    use InstantiableTrait;

    protected static $CREATED_ID = 0;
    protected static $CREATED_ITEM = null;

    protected function setCreatedData(int $id = null, $instance = null)
    {
        static::$CREATED_ID = $id;
        static::$CREATED_ITEM = $instance;
        return $this;
    }

    public static function getCreatedItem()
    {
        return static::$CREATED_ITEM;
    }
}