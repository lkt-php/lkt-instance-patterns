<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\SingleTonTrait;

abstract class AbstractDataContainerInstance
{
    use SingleTonTrait;

    protected $fillers = [];

    /**
     * @param string $name
     * @param string $fillerClass
     * @return $this
     */
    public function addFiller(string $name, string $fillerClass): self
    {
        if (!isset($this->fillers[static::class])){
            $this->fillers[static::class] = [];
        }
        $this->fillers[static::class][$name][] = $fillerClass;
        return $this;
    }


    /**
     * @param string $name
     * @param $args
     * @return $this
     */
    public function run(string $name, $args): self
    {
        foreach ($this->fillers[static::class][$name] as $trigger) {
            $trigger::getInstance($args);
        }

        return $this;
    }
}