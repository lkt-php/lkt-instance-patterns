<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\ParserInterface;

/**
 * Class AbstractParserInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractParserInstance implements ParserInterface
{
    /**
     * @return string
     */
    public function __toString() :string
    {
        return $this->parse();
    }
}