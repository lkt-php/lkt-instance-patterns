<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractReadCrud
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractReadCrud
{
    use InstantiableTrait,
        AutomaticInstanceTrait;
}