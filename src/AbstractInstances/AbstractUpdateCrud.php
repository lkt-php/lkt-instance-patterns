<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractUpdateCrud
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractUpdateCrud extends AbstractAutomaticHandlerInstance
{
    use InstantiableTrait;
}