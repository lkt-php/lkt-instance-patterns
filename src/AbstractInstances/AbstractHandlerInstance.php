<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\HandlerInterface;

/**
 * Class AbstractHandlerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractHandlerInstance implements HandlerInterface
{

}