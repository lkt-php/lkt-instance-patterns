<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractTriggerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractTriggerInstance
{
    use InstantiableTrait;

    protected static $triggers = [];

    /**
     * @param string $triggerName
     * @param string $hookClass
     * @return bool
     */
    public static function register(string $triggerName, string $hookClass): bool
    {
        if (!isset(static::$triggers[static::class])){
            static::$triggers[static::class] = [];
        }
        if (!isset(static::$triggers[static::class][$triggerName])){
            static::$triggers[static::class][$triggerName] = [];
        }

        if (!in_array($hookClass, static::$triggers[static::class][$triggerName], true)){
            static::$triggers[static::class][$triggerName][] = $hookClass;
        }
        return true;
    }

    public static function run(string $triggerName, $args)
    {
        foreach (static::$triggers[static::class][$triggerName] as $trigger) {
            $trigger::getInstance($args);
        }

        return true;
    }
}