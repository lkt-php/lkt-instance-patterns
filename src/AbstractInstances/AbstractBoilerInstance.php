<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractBoilerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
class AbstractBoilerInstance
{
    use InstantiableTrait,
        AutomaticInstanceTrait;

    protected static $MIXES = [];
    protected $args;

    /**
     * @param $args
     */
    public function __construct($args = null)
    {
        $this->args = $args;
    }

    /**
     * @param string $class
     */
    public static function addMix(string $class): void
    {
        $className = static::class;
        if (!isset(static::$MIXES[$className])) {
            static::$MIXES[$className] = [];
        }
        if (!in_array($class, static::$MIXES[$className], true)){
            static::$MIXES[$className][] = $class;
        }
    }

    /**
     * @return array
     */
    public function handle(): array
    {
        $r = [];
        $className = static::class;
        foreach (static::$MIXES[$className] as $mix){
            $data = $mix::getInstance($this->args);
            foreach ($data as $key => $datum){
                $r[$key] = $datum;
            }
        }
        return $r;
    }
}