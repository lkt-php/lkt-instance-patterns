<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class AbstractMixInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
class AbstractMixInstance
{
    use InstantiableTrait,
        AutomaticInstanceTrait;

    protected $args;

    /**
     * @param $args
     */
    public function __construct($args = null)
    {
        $this->args = $args;
    }

    /**
     * @return array
     */
    public function handle(): array
    {
        return [];
    }
}