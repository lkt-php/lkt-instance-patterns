<?php

namespace Lkt\InstancePatterns\AbstractInstances;

use Lkt\InstancePatterns\Interfaces\CheckerInterface;

/**
 * Class AbstractHandlerInstance
 * @package Lkt\InstancePatterns\AbstractInstances
 */
abstract class AbstractCheckerInstance implements CheckerInterface
{

}