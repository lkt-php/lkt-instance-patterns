<?php


namespace Lkt\InstancePatterns\AbstractInstances;


use Lkt\InstancePatterns\Traits\AutomaticInstanceTrait;
use Lkt\InstancePatterns\Traits\InstantiableTrait;


abstract class AbstractDataFillerInstance
{
    use InstantiableTrait,
        AutomaticInstanceTrait;

    protected $args;

    /**
     * @param $args
     */
    public function __construct($args = null)
    {
        $this->args = $args;
    }

    /**
     * @return array
     */
    abstract public function handle(): array;
}