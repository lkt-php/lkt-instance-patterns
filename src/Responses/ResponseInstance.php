<?php

namespace Lkt\InstancePatterns\Responses;

use Lkt\InstancePatterns\Traits\InstantiableTrait;

/**
 * Class ResponseInstance
 * @package Lkt\InstancePatterns\Responses
 */
class ResponseInstance
{
    use InstantiableTrait;

    protected $CODE;
    protected $RESPONSE_DATA = [];

    /**
     * ResponseInstance constructor.
     * @param int $code
     * @param array $responseData
     */
    public function __construct(int $code = 1, array $responseData = [])
    {
        $this->CODE = $code;
        $this->RESPONSE_DATA = $responseData;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->CODE;
    }

    /**
     * @return array
     */
    public function getResponseData(): array
    {
        return $this->RESPONSE_DATA;
    }

    /**
     * @return bool
     */
    public function sendStatusHeader()
    {
        $protocol = $_SERVER['SERVER_PROTOCOL'];
        switch ($this->CODE) {
            case 200:
                header("{$protocol} {$this->CODE} OK");
                return true;
                break;
            case 201:
                header("{$protocol} {$this->CODE} Created");
                return true;
                break;
            case 202:
                header("{$protocol} {$this->CODE} Accepted");
                return true;
                break;
            case 204:
                header("{$protocol} {$this->CODE} No Content");
                return true;
                break;
            case 400:
                header("{$protocol} {$this->CODE} Bad Request");
                return false;
                break;
            case 401:
                header("{$protocol} {$this->CODE} Unauthorized");
                return false;
                break;
            case 403:
                header("{$protocol} {$this->CODE} Forbidden");
                return false;
                break;
            case 404:
                header("{$protocol} {$this->CODE} Not Found");
                return false;
                break;
            case 405:
                header("{$protocol} {$this->CODE} Method Not Allowed");
                return false;
                break;
        }
        return false;
    }
}