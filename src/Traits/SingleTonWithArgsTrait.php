<?php

namespace Lkt\InstancePatterns\Traits;

/**
 * Trait SingleTonWithArgsTrait
 * @package Lkt\InstancePatterns
 */
trait SingleTonWithArgsTrait
{
    /**
     * @var static
     */
    protected static $Instance;

    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        if (!\is_object(self::$Instance)){
            self::$Instance = new static(...$args);
            if (\method_exists(self::$Instance, 'isAutomaticInstance')){
                if (\method_exists(self::$Instance, 'handle')){
                    return self::$Instance->handle();
                }
                if (\method_exists(self::$Instance, 'parse')){
                    return self::$Instance->parse();
                }
            }
        }

        return self::$Instance;
    }
}