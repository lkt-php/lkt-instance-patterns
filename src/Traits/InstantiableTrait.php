<?php

namespace Lkt\InstancePatterns\Traits;

/**
 * Trait InstantiableTrait
 * @package Lkt\InstancePatterns\Traits
 */
trait InstantiableTrait
{
    /**
     * @return static
     */
    public static function getInstance(...$args)
    {
        $r = new static(...$args);

        if (\method_exists($r, 'isAutomaticInstance')){
            if (\method_exists($r, 'handle')){
                return $r->handle();
            }
            if (\method_exists($r, 'parse')){
                return $r->parse();
            }
        }

        return $r;
    }
}