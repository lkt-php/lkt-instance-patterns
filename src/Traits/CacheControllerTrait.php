<?php

namespace Lkt\InstancePatterns\Traits;

/**
 * Trait CacheControllerTrait
 * @package Lkt\InstancePatterns\Traits
 */
trait CacheControllerTrait
{
    protected static $CACHE = [];
    protected static $CACHE_ENABLED = true;

    /**
     * @param string $code
     * @param $data
     * @return int
     */
    public static function store(string $code, $data): int
    {
        static::$CACHE[$code] = $data;
        return 1;
    }

    /**
     * @param string $code
     * @return mixed
     */
    public static function load(string $code)
    {
        if (static::inCache($code)) {
            return static::$CACHE[$code];
        }
        return null;
    }

    /**
     * @param string $code
     * @return bool
     */
    public static function inCache(string $code): bool
    {
        return isset(static::$CACHE[$code]);
    }

    /**
     * @return int
     */
    public static function clear(): int
    {
        static::$CACHE = [];
        return 1;
    }

    /**
     * @param string $code
     * @return int
     */
    public static function clearCode(string $code): int
    {
        if (static::inCache($code)) {
            unset(static::$CACHE[$code]);
            return 1;
        }
        return 2;
    }

    /**
     * @return int
     */
    public static function enable(): int
    {
        static::$CACHE_ENABLED = true;
        return 1;
    }

    /**
     * @return int
     */
    public static function disable(): int
    {
        static::$CACHE_ENABLED = false;
        return 1;
    }

    /**
     * @return bool
     */
    public static function isEnabled(): bool
    {
        return static::$CACHE_ENABLED === true;
    }
}