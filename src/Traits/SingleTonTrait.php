<?php

namespace Lkt\InstancePatterns\Traits;

/**
 * Trait SingleTonTrait
 * @package Lkt\InstancePatterns
 */
trait SingleTonTrait
{
    /**
     * @var static
     */
    protected static $Instance;

    /**
     * @param bool $forceFresh
     * @return static
     */
    public static function getInstance(...$args)
    {
        $forceFresh = (bool)$args[0];
        if ($forceFresh === true || !\is_object(self::$Instance)) {
            self::$Instance = new static();
            if (\method_exists(self::$Instance, 'isAutomaticInstance')) {
                if (\method_exists(self::$Instance, 'handle')){
                    return self::$Instance->handle();
                }
                if (\method_exists(self::$Instance, 'parse')){
                    return self::$Instance->parse();
                }
            }
        }

        return self::$Instance;
    }
}