<?php

namespace Lkt\InstancePatterns\Interfaces;

/**
 * Interface HandlerInterface
 * @package Lkt\InstancePatterns\Interfaces
 */
interface HandlerInterface
{
    /**
     * @return int
     */
    public function handle(): int;
}