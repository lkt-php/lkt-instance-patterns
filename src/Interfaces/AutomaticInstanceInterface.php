<?php

namespace Lkt\InstancePatterns\Interfaces;

/**
 * Interface AutomaticInstanceInterface
 * @package Lkt\InstancePatterns\Interfaces
 */
interface AutomaticInstanceInterface
{
    public function isAutomaticInstance(): bool;
}