<?php

namespace Lkt\InstancePatterns\Interfaces;

/**
 * Interface ParserInterface
 * @package Lkt\InstancePatterns\Interfaces
 */
interface ParserInterface
{
    /**
     * @return string
     */
    public function parse(): string;
}